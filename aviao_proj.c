/* Este programa em C é um trabalho de ICC-BCC, dada pelo professor Rudinei <sobrenome> em 1.2024 
 * Os integrantes do grupo são João Pedro Castelli - 15463450 , Matheus Muzza Pires Ferreira - 15479468, Marcelo Martins Conti - 15474629.
 * Este programa lê um arquivo que guarda informações sobre instâncias passadas do programa, 
 * então lê comandos digitados, realiza as tarefas e, no final, guarda as informações sobre 
 * o voo e as reservas em um arquivo. Na primeira vez, não há arquivo e, portanto, inicia-se o array 
 * de reservas vazio. RR aumenta o array usando realloc e guarda as informações sobre a reserva. 
 * MR modifica a reserva de tal CPF. CA diminui o array e tira a reserva de tal CPF. CR dá print 
 * das informações acerca do CPF digitado. FD irá guardar as informações no arquivo reservas.bin 
 * usando fp como FILE* e fwrite. O arquivo tem a estrutura: os quatro primeiros bytes são um int 
 * que é 1 se o voo já foi fechado, depois um int que diz a quantidade de reservas, então os dados 
 * de todas as reservas no tipo e ordem declarados no struct, depois os dados do voo da mesma forma. 
 * Quando o programa abre outra vez, ele lê os dados do arquivo e coloca no array de reservas e na variável 
 * do struct aviao. Por fim, quando é dado FV, é escrito o int 1 no começo do arquivo, imprimi-se as informações 
 * básicas das reservas e fecha-se o programa. Se o programa é rodado depois de ser fechado o voo, apenas 
 * printa-se "Voo fechado" seguido das informações, assim como especificado no pdf. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *alocar_memoria(int tamanho){
    void *p;
    p=malloc(tamanho);
    if(p==NULL){
        exit(1);
    }
    return(p);
}


struct av{
    int assentos; //importante!!!: após ler o número de assentos na abertura de voo, cada vez que é realizada uma reserva, 
                  //é retirado 1 desse valor, e CA acrescenta 1. Assim, quando assentos chegar a 0, o avião está cheio
    float economica;
    float executiva;
    float valtot ;
}; typedef struct av aviao; //strucut das informações do avião

struct rr{
    char *nome;
    char *sobrenome;
    char cpf[15];
    char dia[3];
    char mes[3];
    char ano[5];
    char nvoo[5];
    char assento[4];
    char classe[10];
    float valor;
    char *origem;
    char *destino;
}; typedef struct rr lista; //lista das informações de uma reserva;

void leitura(char op[3]){ //Leitura da operação que vai ser realizada
    scanf(" %s", op);
}

void fechamento_voo(lista *informacao, aviao voo, int tamanho);

//lê o arquivo e coloca todas as informações contidas nele em váriaves e arrays na memória RAM. Se o arquivo não existe, apenas inicia o array informacao vazio
lista* iniciarArquivo(int *tamanho, aviao *voo){
    FILE *fp;
    lista *informacao;
    char c;
    float valtemp;
    int tamnome, tamsobren, tamorigem, tamdest, assentemp, estaFechado;
    if((fp = fopen("reservas.bin" , "rb")) != NULL){
        fread(&estaFechado, sizeof(int), 1, fp); //inteiro que indica se o voo já <nome da variável
        fread(tamanho, sizeof(int), 1, fp);
        informacao = malloc(sizeof(lista) * *tamanho);
        for(int i = 0; i < *tamanho; i++){
            fread(&tamnome, sizeof(int), 1, fp);
            informacao[i].nome = malloc(tamnome);
            for(int j = 0; j < tamnome; j++){
                fread(&c, sizeof(char), 1, fp); //fread dos chars e vão sendo colocados nas variáveis da struct lista
                informacao[i].nome[j] = c;
            }
            fread(&tamsobren, sizeof(int), 1, fp);
            informacao[i].sobrenome = malloc(tamsobren);
            for(int j = 0; j < tamnome; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].sobrenome[j] = c;
            }
            for(int j = 0; j < 15; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].cpf[j] = c;
            }
            for(int j = 0; j < 3; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].dia[j] = c;
            }
            for(int j = 0; j < 3; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].mes[j] = c;
            }
            for(int j = 0; j < 5; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].ano[j] = c;
            }
            for(int j = 0; j < 5; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].nvoo[j] = c;
            }
            for(int j = 0; j < 4; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].assento[j] = c;
            }
            for(int j = 0; j < 10; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].classe[j] = c;
            }
            fread(&valtemp, sizeof(float), 1, fp);
            informacao[i].valor = valtemp;
            voo[0].valtot += valtemp;
            fread(&tamorigem, sizeof(int), 1, fp);
            informacao[i].origem = malloc(sizeof(char) * tamorigem);
            for(int j = 0; j < tamorigem; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].origem[j] = c;
            }
            fread(&tamdest, sizeof(int), 1, fp);
            informacao[i].destino = malloc(sizeof(char) * tamdest);
            for(int j = 0; j < tamdest; j++){
                fread(&c, sizeof(char), 1, fp);
                informacao[i].destino[j] = c;
            }
        }
        fread(&assentemp, sizeof(int), 1, fp); //informacões sobre a abertura do voo
        voo[0].assentos = assentemp;
        fread(&valtemp, sizeof(float), 1, fp);
        voo[0].economica = valtemp;
        fread(&valtemp, sizeof(float), 1, fp);
        voo[0].executiva = valtemp;
        fclose(fp);
        if(estaFechado == 1){
            fechamento_voo(informacao, *voo, *tamanho);
            exit(0);
        }
    }else{
        *tamanho = 0;
        informacao = malloc(sizeof(lista));
    }
    return informacao;
}

void abrir_voo(aviao *voo){ //Função para abrir o voo
    scanf("%d",&(voo[0].assentos));
    scanf("%f",&(voo[0].economica));
    scanf("%f",&(voo[0].executiva));
    
}

// lê as informações sobre a reserva e coloca-as em uma nova posição do array (realloc)
lista* realizar_reserva(lista *informacao, int tamanho, float *valtot){

    char nome[100];
    char sobrenome[100];
    char cpf[15];
    char dia[3];
    char mes[3];
    char ano[5];
    char nvoo[100];
    char assento[100];
    char classe[10];
    float valor;
    char origem[100];
    char destino[100];

    informacao = (lista*)realloc(informacao, tamanho*sizeof(lista));

    if(informacao==NULL){
        exit(1);
    }

    scanf(" %s", nome); //é necessário alocar memória para arrays com tamanho indefinido
    informacao[tamanho-1].nome = (char*)alocar_memoria((strlen(nome)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].nome, nome);
    
    scanf(" %s", sobrenome);
    informacao[tamanho-1].sobrenome = (char*)alocar_memoria((strlen(sobrenome)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].sobrenome, sobrenome);
    
    scanf(" %s", cpf);
    strcpy(informacao[tamanho-1].cpf, cpf);
    
    scanf(" %s", dia);
    strcpy(informacao[tamanho-1].dia, dia);
    
    scanf(" %s", mes);
    strcpy(informacao[tamanho-1].mes, mes);

    scanf(" %s", ano);
    strcpy(informacao[tamanho-1].ano, ano);

    
    scanf(" %s", nvoo);
    //informacao[tamanho-1].nvoo = (char*)alocar_memoria((strlen(nvoo)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].nvoo, nvoo);
    
    
    scanf(" %s", assento);
    //informacao[tamanho-1].assento = (char*)alocar_memoria((strlen(assento)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].assento, assento);


    
    scanf(" %s", classe);
    strcpy(informacao[tamanho-1].classe, classe);

    
    scanf(" %f", &valor);
    informacao[tamanho-1].valor = valor;
    *valtot+=valor; //adiciona-se o valor da reserva ao valor total do voo
    
    
    scanf(" %s", origem);
    informacao[tamanho-1].origem = (char*)alocar_memoria((strlen(origem)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].origem, origem);

    scanf(" %s", destino);
    informacao[tamanho-1].destino = (char*)alocar_memoria((strlen(destino)+1)*sizeof(char));
    strcpy(informacao[tamanho-1].destino, destino);

    return informacao;
}

//dá free, ou fri para os mais íntimos, nos ponteiros dentro do array e no array em si
void darFri(lista *informacao, int tamanho){
    for(int i = 0; i < tamanho; i++){
        free(informacao[i].nome);
        free(informacao[i].sobrenome);
        free(informacao[i].origem);
        free(informacao[i].destino);
    }
    free(informacao);
    return;
}

//modifica algumas variáveis da reserva do CPF lido
void modificar_reserva(lista *informacao, int tamanho){
    char cpf[15];
    char nome[100];
    char sobrenome[100];
    char assento[100];
    int k;
    scanf(" %s",cpf);

    for(int i=0;i<tamanho;i++){
        if(strcmp(cpf,informacao[i].cpf)==0){
            k=i;
        }
        break;
    }

    scanf(" %s",nome);
    scanf(" %s",sobrenome);
    scanf(" %s",cpf);
    scanf(" %s",assento);

    free(informacao[k].nome);
    free(informacao[k].sobrenome);

    informacao[k].nome=alocar_memoria(1*(strlen(nome)+1));
    informacao[k].sobrenome=alocar_memoria(1*(strlen(sobrenome)+1));

    strcpy(informacao[k].nome,nome);
    strcpy(informacao[k].sobrenome,sobrenome);
    strcpy(informacao[k].assento,assento);
    strcpy(informacao[k].cpf,cpf); 

    printf("\nReserva Modificada:\n");
    printf("%s\n",informacao[k].cpf);
    printf("%s ",informacao[k].nome);
    printf("%s\n",informacao[k].sobrenome);
    printf("%s/",informacao[k].dia);
    printf("%s/",informacao[k].mes);
    printf("%s\n",informacao[k].ano);
    printf("Voo: ");
    printf("%s\n",informacao[k].nvoo);
    printf("Assento: ");
    printf("%s\n",informacao[k].assento);
    printf("Classe: ");
    printf("%s\n",informacao[k].classe);      
    printf("Trecho: ");
    printf("%s ",informacao[k].origem);
    printf("%s\n",informacao[k].destino);
    printf("Valor: ");
    printf("%.2f",informacao[k].valor);
    printf("\n--------------------------------------------------\n");

}

//lê o cpf. cria um novo array de listas. vai copiando as var da antiga pra nova lista. quando acha o CPF, pula ele e começa a copiar as var da posição seguinte, assim excluindo a posição do array com aquele CPF.
void *cancelar_reserva(lista *informacao, int tamanho, float *valtot){
    char cpf[15];
    scanf(" %s",cpf);
    int k=0;
    lista *novainformacao = (lista*)alocar_memoria((tamanho-1)*sizeof(lista));

    for(int i=0;i<tamanho-1;i++){
    
        if(strcmp(cpf,informacao[i].cpf)==0){
            *valtot -= informacao[i].valor;
            k=1;
        }  
        novainformacao[i].nome = informacao[i+k].nome;
        novainformacao[i].sobrenome = informacao[i+k].sobrenome;
        strcpy(novainformacao[i].cpf, informacao[i+k].cpf);
        strcpy(novainformacao[i].dia, informacao[i+k].dia);
        strcpy(novainformacao[i].mes, informacao[i+k].mes);
        strcpy(novainformacao[i].ano, informacao[i+k].ano);
        strcpy(novainformacao[i].nvoo, informacao[i+k].nvoo); //tenho minhas duvidas nesse tamanho
        strcpy(novainformacao[i].assento, informacao[i+k].assento);
        strcpy(novainformacao[i].classe, informacao[i+k].classe);
        novainformacao[i].valor = informacao[i+k].valor;
        novainformacao[i].origem = informacao[i+k].origem;
        novainformacao[i].destino = informacao[i+k].destino;

    }

    darFri(informacao,tamanho);
    printf("%s", novainformacao[0].cpf);
    return(novainformacao);
}

void printarInformacao(lista *informacao, int consulta){
	printf("\n%s\n%s %s\n%s/%s/%s\nVoo: %s\nAssento: %s\nClasse: %s\nTrecho: %s %s\nValor: %.2f", informacao[consulta].cpf, informacao[consulta].nome, informacao[consulta].sobrenome, informacao[consulta].dia, informacao[consulta].mes, informacao[consulta].ano, informacao[consulta].nvoo, informacao[consulta].assento, informacao[consulta].classe, informacao[consulta].origem, informacao[consulta].destino, informacao[consulta].valor);
    printf("\n--------------------------------------------------\n");
}

//recebe o array das informacoes das reservas e o tamanho dele, le o cpf que deve ser consultado, localiza onde esta a reserva com
//tal cpf e chama a funcao para printar as informacoes dessa reserva
void consultar_reserva(lista *informacao, int tamanho){
	char cpftemp[16];
	scanf(" %s", cpftemp);
	int consulta; // guarda a posiçao no array informacao que deve ser printado

	for(int i = 0; i < tamanho; i++){
		if(strcmp(informacao[i].cpf, cpftemp) == 0){
			consulta = i;
			break;
		}
	}
	printarInformacao(informacao, consulta);
}

void escreverArquivo(int tamanho, lista *informacao, aviao voo, int intezero);

//ao fechar o voo, printa algumas informações sobre as reservas, escreve no arquivo e fecha o programa 
void fechamento_voo(lista *informacao, aviao voo,  int tamanho){
    printf("Voo Fechado!");
	for(int i = 0; i < tamanho; i++){
		printf("\n\n%s\n%s %s\n%s", informacao[i].cpf, informacao[i].nome, informacao[i].sobrenome, informacao[i].assento);
	}
	printf("\n\nValor Total: %.2f", voo.valtot);
    printf("\n--------------------------------------------------\n");
    escreverArquivo(tamanho, informacao, voo, 1);
	darFri(informacao, tamanho);
	return;
}

//escreve as informações do array de reservas e do voo no arquivo reservas.bin; o header contem se já foi fechado o voo e o tamanho do array, ou seja, a quant de reservas
//intezero é o primeiro int escrito no arquivo. Caso a função seja chamada por fechamento_voo, é 1, se for por fim_dia, é 0
void escreverArquivo(int tamanho, lista *informacao, aviao voo, int intezero){
    FILE *fp;
    char c;
    int tamnome, tamsobren, tamorigem, tamdest, assentemp;
    float valtemp;
    if((fp = fopen("reservas.bin", "wb")) == NULL){
        printf("erro em abrir arquivo\n");
        exit(1);
    }
    fwrite(&intezero, sizeof(int), 1, fp);
    fwrite(&tamanho, sizeof(int), 1, fp);
    for(int i = 0; i < tamanho; i++){
        tamnome = strlen(informacao[i].nome) + 1;
        fwrite(&tamnome, sizeof(int), 1, fp);
        for(int j = 0; j < tamnome; j++){
            c = informacao[i].nome[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        tamsobren = strlen(informacao[i].sobrenome) + 1;
        fwrite(&tamsobren, sizeof(int), 1, fp);
        for(int j = 0; j < tamnome; j++){
            c = informacao[i].sobrenome[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 15; j++){
            c = informacao[i].cpf[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 3; j++){
            c = informacao[i].dia[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 3; j++){
            c = informacao[i].mes[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 5; j++){
            c = informacao[i].ano[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 5; j++){
            c = informacao[i].nvoo[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 4; j++){
            c = informacao[i].assento[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        for(int j = 0; j < 10; j++){
            c = informacao[i].classe[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        valtemp = informacao[i].valor;
        fwrite(&valtemp, sizeof(float), 1, fp);
        tamorigem = strlen(informacao[i].origem) + 1;
        fwrite(&tamorigem, sizeof(int), 1, fp);
        for(int j = 0; j < tamorigem; j++){
            c = informacao[i].origem[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
        tamdest = strlen(informacao[i].destino) + 1;
        fwrite(&tamdest, sizeof(int), 1, fp);
        for(int j = 0; j < tamdest; j++){
            c = informacao[i].destino[j];
            fwrite(&c, sizeof(char), 1, fp);
        }
    }
    assentemp = voo.assentos;
    fwrite(&assentemp, sizeof(int), 1, fp);
    valtemp = voo.economica;
    fwrite(&valtemp, sizeof(float), 1, fp);
    valtemp = voo.executiva;
    fwrite(&valtemp, sizeof(float), 1, fp);
    fclose(fp);
}

//finaliza o dia. escreva as informacões atuais no arquivo, dá free em tudo e fecha o programa
void fim_dia( int tamanho, float valtot, lista *informacao, aviao voo){
    printf("Fechamento do dia:\n");
    printf("Quantidade de reservas: %d\n", tamanho);
    printf("Posição: %.2f", valtot);
    escreverArquivo(tamanho, informacao, voo, 0);
    darFri(informacao, tamanho);
    printf("\n--------------------------------------------------\n");
}


int main(){

    char op[3]; //operação, ou seja, o comando de 2 caracteres (+ \0)
    int tamanho = 0;

    lista *informacao = NULL; //array de listas, que contêm as informações de cada uma das reservas
    aviao voo; //struct das informações do voo
    voo.valtot=0;
    informacao = iniciarArquivo(&tamanho, &voo);

    leitura(op); //ler a operação
    
    while(strcmp(op,"FV") != 0){ //enquanto não for FV(fechamento do voo)
    
        if(strcmp(op, "CR") == 0){

            consultar_reserva(informacao, tamanho);

        }else if(strcmp(op, "MR") == 0){

            if(voo.assentos==0){
               printf("Voo Fechado!");
	           for(int i = 0; i < tamanho; i++){
		            printf("\n\n%s\n%s %s\n%s", informacao[i].cpf, informacao[i].nome, informacao[i].sobrenome, informacao[i].assento);
	            }
                printf("\n\nValor Total: %.2f", voo.valtot);
                printf("\n--------------------------------------------------\n");
                

            }else{
                modificar_reserva(informacao,tamanho);
            }

            

        }else if(strcmp(op, "FD" )==0){

            fim_dia(tamanho, voo.valtot, informacao, voo);
            return 0;
            
            
        }else if(strcmp(op, "RR")==0){
            if(voo.assentos==0){
               printf("Voo Fechado!");
	           for(int i = 0; i < tamanho; i++){
		            printf("\n\n%s\n%s %s\n%s", informacao[i].cpf, informacao[i].nome, informacao[i].sobrenome, informacao[i].assento);
	            }
                printf("\n\nValor Total: %.2f", voo.valtot);
                printf("\n--------------------------------------------------\n");
                

            }else{
                voo.assentos--;
                tamanho++;
                informacao = realizar_reserva(informacao, tamanho, &voo.valtot);
                if(voo.assentos==0){
                    fechamento_voo(informacao, voo, tamanho);
                    return 0;
                }
            }
        
        }else if(strcmp(op, "AV" )==0){

            abrir_voo(&voo);
            if(voo.assentos == 0){
                fechamento_voo(informacao, voo, tamanho);
            }
            
        }else if(strcmp(op,"CA") == 0){

            if(voo.assentos==0){
               printf("Voo Fechado!");
	           for(int i = 0; i < tamanho; i++){
		            printf("\n\n%s\n%s %s\n%s", informacao[i].cpf, informacao[i].nome, informacao[i].sobrenome, informacao[i].assento);
	            }
                printf("\n\nValor Total: %.2f", voo.valtot);
                printf("\n--------------------------------------------------\n");
                
            }else{
                voo.assentos++;
                cancelar_reserva(informacao,tamanho, &voo.valtot);
                tamanho--;
            }
        }
        leitura(op); //ler a operação
    }
    fechamento_voo(informacao, voo, tamanho);
    return 0;
}
